{ lib
, stdenv
, fetchFromGitHub
, rgbds
}:
stdenv.mkDerivation {
	pname = "gb-bootroms";
	version = "0.1.0+6232573bc";
	src = fetchFromGitHub {
		owner = "ISSOtm";
		repo = "gb-bootroms";
		rev = "6232573bc6592df17cdbce878c418e79d8355b68";
		hash = "sha512-GReDdkPDdndC8GCcmLCYFaLPbkaTH9Ff3+GKWSUwrnJJTmyROVUiewhRAp+x2YTQvhSttKgDko8BKhBSR0Ii4A==";
		fetchSubmodules = true;
	};
	nativeBuildInputs = [ stdenv rgbds ];
	dontConfigure = true;
	installPhase = ''
		mkdir -p -v $out
		for f in bin/dmg.bin bin/cgb.bin; do
			cp -a -v -t $out $f
		done
	'';

	meta = {
		description = "(Assembled) disassemblies of the different Game Boy boot ROMs";
		homepage = "https://github.com/ISSOtm/gb-bootroms";
		license = lib.licenses.unfree; # decompilation of closed source binaries
		platforms = lib.platforms.all;
	};
}
