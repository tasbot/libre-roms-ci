#!/usr/bin/env sh
set -e
x="$(dirname "$0")/build_and_check_hash.sh"
for a in $(awk '{ print $1 }' expected_hashes.tsv); do NIXPKGS_ALLOW_UNFREE=1 "$x" "$a"; done
