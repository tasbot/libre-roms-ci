{ lib
, stdenv
, fetchFromGitHub
, rgbds
}:
stdenv.mkDerivation {
	pname = "BullyGB";
	version = "1.2+e24fe6fd7";
	srcs = [
		(fetchFromGitHub {
			name = "BullyGB";
			owner = "Hacktix";
			repo = "BullyGB";
			rev = "e24fe6fd7f3fbc6021e3ee7f0f29e9166fce5937";
			hash = "sha512-gh0V/VEIZHKPji7Md9jgqPkbRm8GtcIiU15BugBlGOUb2qA77oLzm7lWaz1cWnQkw8g18vBnoJSdWl78PpxgJw==";
		})
		(builtins.path { path = ./img; name = "img"; })
	];
	nativeBuildInputs = [ stdenv rgbds ];
	setSourceRoot = ''
		mkdir -p source
		cp -a -t source BullyGB/*
		cp -a -t source img/*
		export sourceRoot=source
	'';
	dontConfigure = true;
	installPhase = ''
		mkdir -p -v $out
		for f in bully.* expected_*.png; do
			cp -a -v -t $out $f
		done
	'';

	meta = {
		description = "A modular Gameboy test framework and a lot of potentially emulator-breaking test cases.";
		homepage = "https://github.com/Hacktix/BullyGB";
		license = lib.licenses.mit;
		platforms = lib.platforms.all;
	};
}
