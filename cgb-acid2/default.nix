{ lib
, stdenv
, fetchFromGitHub
, rgbds
}:
stdenv.mkDerivation {
	pname = "cgb-acid2";
	version = "1.1+04c6ca40c";
	src = fetchFromGitHub {
		owner = "mattcurrie";
		repo = "cgb-acid2";
		rev = "04c6ca40cf75b6a93513fe596de4ab797efaff97";
		hash = "sha512-J03+Wial9VoeA5uMzpB6jovqBr/YuRGP4K3QtLfkGHxnd26Q2TOecO7dLo2R1dsNImV0xj7p+Wzzoc6zHyhXAA==";
		fetchSubmodules = true;
	};
	nativeBuildInputs = [ stdenv rgbds ];
	dontConfigure = true;
	installPhase = ''
		mkdir -p -v $out
		for f in build/* img/*; do
			cp -a -v -t $out $f
		done
	'';

	meta = {
		description = "The Acid2 test, now for Game Boy Color!";
		homepage = "https://github.com/mattcurrie/cgb-acid2";
		license = lib.licenses.mit;
		platforms = lib.platforms.all;
	};
}
