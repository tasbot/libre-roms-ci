#!/usr/bin/env sh
set -e
cd "$(dirname "$0")"
nix-build --pure -A "$1"
if (grep -P "^$1\t" expected_hashes.tsv | fgrep -q "$(realpath result)"); then
	printf "CA checksum for %s matched\n" "$1"
else
	printf "CA checksum for %s unstable!\n" "$1"
	exit 1
fi
