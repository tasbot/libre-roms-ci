{ system ? builtins.currentSystem
, pkgs ? import (builtins.fetchTarball {
	url = "https://github.com/NixOS/nixpkgs/archive/branch-off-24.11.tar.gz"; # no plain tag since they're "still working on it"
	sha256 = "1gx0hihb7kcddv5h0k7dysp2xhf1ny0aalxhjbpj2lmvj7h9g80a";
}) { inherit system; }
, lib ? pkgs.lib
, stdenv ? pkgs.stdenv.override { config = pkgs.config // {
	contentAddressedByDefault = true;
	strictDepsByDefault = true;
}; }
, fetchFromGitHub ? pkgs.fetchFromGitHub
, fetchpatch ? pkgs.fetchpatch
, fetchzip ? pkgs.fetchzip
, bison ? pkgs.bison
, flex ? pkgs.flex
, libpng ? pkgs.libpng
, pkg-config ? pkgs.pkg-config
, python3Packages ? pkgs.python3Packages
, which ? pkgs.which
}:
let
	python = python3Packages.python;
	rgbds052 = import ./rgbds { inherit lib stdenv fetchFromGitHub bison flex libpng pkg-config; };
	rgbds041 = rgbds052.overrideAttrs (finalAttrs: {
		version = "0.4.1";
		src = finalAttrs.src.override { hash = "sha512-1QgPI1TW9CUzRTuYxkPFc3xN6Oyw5tiQxq1BYNUa6JGgkG2D6yM2s8GLiUF8vkG+wghitIX0pO3bAWH2a1ruhg=="; };
	});
	callPackage = lib.callPackageWith { inherit lib stdenv fetchFromGitHub fetchpatch fetchzip which; };
in {
	inherit rgbds041 rgbds052;
	blargg-gb-tests = callPackage ./blargg-gb-tests {}; # "unfree", may not distribute/host CI artifacts
	BullyGB = callPackage ./BullyGB { rgbds = rgbds052; };
	CasualPokePlayer-test-roms = callPackage ./CasualPokePlayer-test-roms { rgbds = rgbds052; inherit python; };
	cgb-acid-hell = callPackage ./cgb-acid-hell { rgbds = rgbds052; };
	cgb-acid2 = callPackage ./cgb-acid2 { rgbds = rgbds052; };
	dmg-acid2 = callPackage ./dmg-acid2 { rgbds = rgbds052; };
	Gambatte-testroms = callPackage ./Gambatte-testroms { inherit python; };
	gb-bootroms = callPackage ./gb-bootroms { rgbds = rgbds052; }; # "unfree", may not distribute/host CI artifacts
	homebrewAndDemos = import ./homebrewAndDemos { inherit callPackage python3Packages rgbds041; }; # various licences
	licensedGames = import ./licensedGames { inherit callPackage rgbds052; }; # all derivations "unfree", may not distribute/host CI artifacts
	mealybug-tearoom-tests = callPackage ./mealybug-tearoom-tests { rgbds = rgbds041; };
	rgbds = rgbds052;
	rtc3test = callPackage ./rtc3test { rgbds = rgbds041; };
	SameSuite = callPackage ./SameSuite { rgbds = rgbds052; };
}
