{ lib
, stdenv
, fetchFromGitHub
, rgbds # >= 0.5.1
, which
}:
stdenv.mkDerivation (finalAttrs: {
	pname = "pokecrystal";
	version = "0.1.0+7a03fecc3";
	src = fetchFromGitHub {
		owner = "pret";
		repo = finalAttrs.pname;
		rev = "7a03fecc38ab4fe2e44525b56dabc5a05a6d8633";
		hash = "sha512-mMlg5d4XyagUx+8e+ufP9wQGgMjXaeKLsJAnpwYIfzLS6Dnfc3K2yg1xY/SMUXcMDve1BWqWhZXXPQzE4J+64Q==";
	};
	nativeBuildInputs = [ stdenv rgbds which ];
	dontConfigure = true;
	postBuild = ''
		make compare
	'';
	installPhase = ''
		mkdir -p -v $out
		for f in *.gbc; do
			cp -a -v -t $out $f
		done
	'';

	meta = {
		description = "(Assembled) disassembly of Pokémon Crystal";
		homepage = "https://github.com/pret/pokecrystal";
		license = lib.licenses.unfree; # decompilation of closed source binaries
		platforms = lib.platforms.all;
	};
})
