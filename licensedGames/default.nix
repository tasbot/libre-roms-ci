{ callPackage
, rgbds052
}: {
	pokecrystal = callPackage ./pokecrystal { rgbds = rgbds052; };
	pokegold = callPackage ./pokegold { rgbds = rgbds052; };
	pokered = callPackage ./pokered { rgbds = rgbds052; };
	pokeyellow = callPackage ./pokeyellow { rgbds = rgbds052; };
}
