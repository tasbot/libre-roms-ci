{ lib
, stdenv
, fetchFromGitHub
, rgbds # >= 0.5.1
, which
}:
stdenv.mkDerivation (finalAttrs: {
	pname = "pokered";
	version = "0.1.0+9c93fb0b7";
	src = fetchFromGitHub {
		owner = "pret";
		repo = finalAttrs.pname;
		rev = "9c93fb0b7559e9d657a91d2b508ecebe58eee544";
		hash = "sha512-tQbbI6YVs4KS4NjXuLhHHIg5sme3Rl8O/zixLREDINCmUOscrEltyvjbNWgQwxpkE1KGyvprNTdDKy1BFgwGMA==";
	};
	nativeBuildInputs = [ stdenv rgbds which ];
	dontConfigure = true;
	postBuild = ''
		make compare
	'';
	installPhase = ''
		mkdir -p -v $out
		for f in *.gbc; do
			cp -a -v -t $out $f
		done
	'';

	meta = {
		description = "(Assembled) disassembly of Pokémon Red/Blue";
		homepage = "https://github.com/pret/pokered";
		license = lib.licenses.unfree; # decompilation of closed source binaries
		platforms = lib.platforms.all;
	};
})
