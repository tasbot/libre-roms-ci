{ lib
, stdenv
, fetchFromGitHub
, rgbds
}:
stdenv.mkDerivation {
	pname = "rtc3test";
	version = "4.0+80ae792bf";
	srcs = [
		(fetchFromGitHub {
			name = "rtc3test";
			owner = "aaaaaa123456789";
			repo = "rtc3test";
			rev = "80ae792bf1b3c3387929b912e6df001af3511e24";
			hash = "sha512-/x5HqbZMn5qUzMF3Ml3Y+KzZen9tcTpbZaNHZU7o1TnaxmxV1hXuegrGvWSlVMNXdZMd2/e6fW79Q+DdLLMDZA==";
		})
		(builtins.path { path = ./img; name = "img"; })
	];
	nativeBuildInputs = [ stdenv rgbds ];
	setSourceRoot = ''
		mkdir -p source
		cp -a -t source rtc3test/*
		cp -a -t source img/*
		export sourceRoot=source
	'';
	dontConfigure = true;
	installPhase = ''
		mkdir -p -v $out
		for f in rtc3test.* expected_*.png; do
			cp -a -v -t $out $f
		done
	'';

	meta = {
		description = "MBC3 RTC test for Game Boy flash carts and emulators";
		homepage = "https://github.com/aaaaaa123456789/rtc3test";
		license = lib.licenses.unlicense;
		platforms = lib.platforms.all;
	};
}
