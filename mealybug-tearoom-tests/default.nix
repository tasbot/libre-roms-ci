{ lib
, stdenv
, fetchFromGitHub
, rgbds
}:
stdenv.mkDerivation {
	pname = "mealybug-tearoom-tests";
	version = "0.1.0+70e88fb90";
	src = fetchFromGitHub {
		owner = "mattcurrie";
		repo = "mealybug-tearoom-tests";
		rev = "70e88fb90b59d19dfbb9c3ac36c64105202bb1f4";
		hash = "sha512-alyhWJoqF5pdNDjIpU6g5KB75uZqNEENmLSDNeduT63m5+OvIHRA02fslFGMNkaDd0nzERxvyucly+4QTi8WfQ==";
		fetchSubmodules = true;
	};
	nativeBuildInputs = [ stdenv rgbds ];
	dontConfigure = true;
	installPhase = ''
		mkdir -p -v $out
		for f in build expected; do
			cp -a -v -t $out $f
		done
	'';

	meta = {
		description = "Game Boy emulator test ROMs";
		homepage = "https://github.com/mattcurrie/mealybug-tearoom-tests";
		license = lib.licenses.mit;
		platforms = lib.platforms.all;
	};
}
