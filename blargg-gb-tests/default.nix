{ lib
, stdenv
, fetchzip
}:
stdenv.mkDerivation {
	pname = "blargg-gb-tests";
	version = "0.1.0+20101202";
	srcs = [
		(fetchzip {
			name = "cgb_sound";
			url = "https://gbdev.gg8.se/files/roms/blargg-gb-tests/cgb_sound.zip";
			hash = "sha512-JyrcWhnvyCHf8Timt554wOLwBZTKLMgaJR4Lmp2/6eZiY7ygLTqsp5Z7AKjL3BAFsuKh8D4/2hXk0YxODz2jlg==";
		})
		(fetchzip {
			name = "cpu_instrs";
			url = "https://gbdev.gg8.se/files/roms/blargg-gb-tests/cpu_instrs.zip";
			hash = "sha512-2NSw95+PDY5sUD3GrWKNu0t0Wcfq2s3Hn649wK51uil/6T9TtVpZXltisuf1zHsxNKFBllRH2IN3q978UL2HJQ==";
		})
		(fetchzip {
			name = "dmg_sound";
			url = "https://gbdev.gg8.se/files/roms/blargg-gb-tests/dmg_sound.zip";
			hash = "sha512-QE/PSSX1K8ldGUzFoE4t8gsd5Ry4LcKSWi23elgTIlbyDA5/xZpcFy8Ekardeo92WQAVmMgblmEZ0TvVblpItg==";
		})
		(fetchzip {
			name = "halt_bug";
			url = "https://gbdev.gg8.se/files/roms/blargg-gb-tests/halt_bug.zip";
			hash = "sha512-yu7W0mEW0oxhkA9/440W57/1MthmkVHsV8uEen0uNrqjFkEGgXxpzWUZRzUhwe4WFQAGKW5IOWPWpQcoPQw6tA==";
		})
		(fetchzip {
			name = "instr_timing";
			url = "https://gbdev.gg8.se/files/roms/blargg-gb-tests/instr_timing.zip";
			hash = "sha512-oHoxcGL3rmUhE2IDQHCCCOAdFyVdB3ul++XIzFfxcqBX/BaPcUStfyNpfOJLzoykTa11pFuPOi8XF0d+JsOnVQ==";
		})
		(fetchzip {
			name = "interrupt_time";
			url = "https://gbdev.gg8.se/files/roms/blargg-gb-tests/interrupt_time.zip";
			hash = "sha512-O/8/dQuHx1UQA5QrdWTMmEWzQaArSn38ZmzOTWwCwBHCZ1sG/rytV4NxuL/cJ6/IBNR4ej3ow5ATViDdTXYaeQ==";
		})
		(fetchzip {
			name = "mem_timing";
			url = "https://gbdev.gg8.se/files/roms/blargg-gb-tests/mem_timing.zip";
			hash = "sha512-2HaHSGZ/rgsFnhLa14dIHeCjo4q2DxqEKBOlrygmeVCZty+vSCMnxzm3Qv2hQr6ykMGdPfNH+IeAyhdnfyOmOA==";
		})
		(fetchzip {
			name = "mem_timing-2";
			url = "https://gbdev.gg8.se/files/roms/blargg-gb-tests/mem_timing-2.zip";
			hash = "sha512-GREEPvDnrH+uM2ACOxe6yh2VPxrC4a/E6igDMg9cbFfDsHc7feSltMi88Javjnsc30n1UIwmmGEw2olD8AGX8g==";
		})
		(fetchzip {
			name = "oam_bug";
			url = "https://gbdev.gg8.se/files/roms/blargg-gb-tests/oam_bug.zip";
			hash = "sha512-cMo3G6/vNlQcXqa+z1hwOkPBMt+wfts+X+PDd+5JiJW8tq5ZdRmRtzZBVRywOY/85Vyk69DL+SUVsOYRZ3uGoA==";
		})
	];
	nativeBuildInputs = [ stdenv ];
	setSourceRoot = ''
		mkdir -p source
		for d in cgb_sound cpu_instrs dmg_sound halt_bug instr_timing interrupt_time mem_timing mem_timing-2 oam_bug; do
			cp -a -t source $d
		done
		export sourceRoot=source
	'';
	dontPatch = true;
	dontConfigure = true;
	dontBuild = true;
	installPhase = ''
		mkdir -p $out
		cp -a -t $out *
	'';

	meta = {
		description = "Blargg's GB testroms";
		homepage = "https://gbdev.gg8.se/files/roms/blargg-gb-tests/";
		license = lib.licenses.unfree; # binaries with no included license
		platforms = lib.platforms.all;
	};
}
