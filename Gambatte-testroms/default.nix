{ lib
, stdenv
, fetchFromGitHub
, python
}:
stdenv.mkDerivation {
	pname = "Gambatte-testroms";
	version = "0.1.0+745d109aa";
	src = fetchFromGitHub {
		owner = "pokemon-speedrunning";
		repo = "gambatte-core";
		rev = "745d109aa8740d5f680d901eb834ded4a647590e";
		hash = "sha512-KuaK+yel3V8SNkPBU4vtUKJTbvesqKyMJT8Rd8xZr2XYma4v8LIgJnCsS2deD12LkcyNKNrYudE6FB9GD3Az/A==";
	};
	sourceRoot = "source/test";
	nativeBuildInputs = [ stdenv python ];
	dontConfigure = true;
	buildPhase = ''
		sh scripts/assemble_tests.sh
	'';
	installPhase = ''
		cd hwtests
		find . -type f \( -iname "*.gb" -o -iname "*.gbc" -o -iname "*.png" \) -exec install -Dv "{}" "$out/{}" \;
	'';

	meta = {
		description = "Testroms from the Gambatte project";
		homepage = "https://github.com/pokemon-speedrunning/gambatte-core";
		license = lib.licenses.gpl2Only;
		platforms = lib.platforms.all;
	};
}
