{ lib
, stdenv
, fetchFromGitHub
, rgbds
}:
stdenv.mkDerivation {
	pname = "SameSuite";
	version = "0.1.0+eb48ef5a9";
	src = fetchFromGitHub {
		owner = "LIJI32";
		repo = "SameSuite";
		rev = "eb48ef5a9015ae2a0be7e6bf458bd5ef551a69be";
		hash = "sha512-M3CCvCWh2PWqRRJ8XJKtuTYRytmQnkildUBIxUBZKotXYa8T8wsjAT5BxUK5Rj2+U+YetDfJpLcmPzMFFEnjHg==";
	};
	nativeBuildInputs = [ stdenv rgbds ];
	dontConfigure = true;
	installPhase = ''
		mkdir -p -v $out
		for f in */*.gb */*.sym; do
			cp -a -v -t $out $f
		done
	'';

	meta = {
		description = "A suite of Game Boy tests, used for hardware research and emulator testing";
		homepage = "https://github.com/LIJI32/SameSuite";
		license = lib.licenses.mit;
		platforms = lib.platforms.all;
	};
}
