{ lib
, stdenv
, fetchFromGitHub
, bison
, flex
, libpng
, pkg-config
}:
stdenv.mkDerivation (finalAttrs: {
	pname = "rgbds";
	version = "0.5.2";
	src = fetchFromGitHub {
		owner = "gbdev";
		repo = "rgbds";
		rev = "refs/tags/v${finalAttrs.version}";
		hash = "sha512-/lMzzGEgnAwsnB0Kav204pamYa9OWc8EE+ZH0xWV1OR7LkEp1GHAfNgF/Z47usuBIhF+PVAaFmkVko4zhRMkcQ==";
	};
	nativeBuildInputs = [ bison flex pkg-config ];
	buildInputs = [ libpng ];
	installFlags = [ "PREFIX=${builtins.placeholder "out"}" ];

	meta = {
		description = "A free assembler/linker package for the Game Boy and Game Boy Color";
		homepage = "https://rgbds.gbdev.io";
		license = lib.licenses.mit;
		platforms = lib.platforms.all;
	};
})
