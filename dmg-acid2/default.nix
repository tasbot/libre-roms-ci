{ lib
, stdenv
, fetchFromGitHub
, rgbds
}:
stdenv.mkDerivation {
	pname = "dmg-acid2";
	version = "1.0+8a98ce731";
	src = fetchFromGitHub {
		owner = "mattcurrie";
		repo = "dmg-acid2";
		rev = "8a98ce731f96dde032ffb22ec36dc985d78fdb18";
		hash = "sha512-HeQbqH14JmXZ4RY38aA9HATenFTg1xjIuWSLWXL8AZqcS8Y7l29Z1NnNPpUk/CAVLF2lUz/h1iko5p39pRAv8A==";
		fetchSubmodules = true;
	};
	nativeBuildInputs = [ stdenv rgbds ];
	dontConfigure = true;
	installPhase = ''
		mkdir -p -v $out
		for f in build/* img/*; do
			cp -a -v -t $out $f
		done
	'';

	meta = {
		description = "The Acid2 test, now for the original Game Boy!";
		homepage = "https://github.com/mattcurrie/dmg-acid2";
		license = lib.licenses.mit;
		platforms = lib.platforms.all;
	};
}
