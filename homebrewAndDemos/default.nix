{ callPackage
, python3Packages
, rgbds041
}: {
	libbet = callPackage ./libbet { inherit python3Packages; rgbds = rgbds041; };
}
