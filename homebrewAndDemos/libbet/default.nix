{ lib
, stdenv
, fetchFromGitHub
, fetchpatch
, python3Packages
, rgbds # < 0.5
, which
}:
stdenv.mkDerivation (finalAttrs: {
	pname = "libbet";
	version = "0.07";
	src = fetchFromGitHub {
		owner = "pinobatch";
		repo = finalAttrs.pname;
		rev = "refs/tags/v${finalAttrs.version}";
		postFetch = ''sed '/GBEMU :=/c GBEMU := echo' -i $out/makefile''; # what is this bruh, see https://github.com/pinobatch/libbet/commit/5dfbf80f25566104d45490a24de35bea406db1ff
		hash = "sha512-RhgjbUwam4gX5/4fFnoy7MmKRMmrVTBCY3kigX+efU7x6B9up/EOXRD257rpzTRnjCdKwRnoyIcJ87xPj9COxQ==";
	};
	patches = [
		(fetchpatch { # an update to Pillow broke a script
			url = "https://github.com/pinobatch/libbet/commit/f9c71574c167b75cbedcae60a376713697191096.patch";
			hash = "sha512-j+lHVAipSOBYdbjLOgp6ohzsimjpote3AUvyWqfe5gfcNQ5bp7Ddz2xQEMtxfwMyx0WMtMOIkNnVDdNyyxZGCw==";
		})
	];
	python = python3Packages.python.withPackages (p: [ p.pillow ]);
	nativeBuildInputs = [ finalAttrs.python rgbds ];
	dontConfigure = true;
	buildTargets = [ "dist" ];
	installPhase = ''
		mkdir -p $out
		cp -vt $out libbet.*
	'';

	meta = {
		description = "Game Boy port of the NO$ sample game Magic Floor"; # original: https://problemkaputt.de/magicflr.htm
		homepage = "https://github.com/pinobatch/libbet";
		license = lib.licenses.zlib;
		platforms = lib.platforms.all;
	};
})
